//
//  ViewController.swift
//  MapKitApp
//
//  Created by AlexM on 11/12/14.
//  Copyright (c) 2014 AlexM. All rights reserved.
//

import UIKit
import MapKit

var mapview: MKMapView!

class ViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var map: MKMapView!
    
    var titulo = UITextField()
    var subtitulo = UITextField()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Se hace esta igualdad para poder crear el mapa en el viewDidLoad sin que explote
        mapview = map
        
        self.refreshPosition()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func refreshAction(sender: AnyObject) {
        self.refreshPosition()
    }

    @IBAction func removeAction(sender: AnyObject) {
        mapview.removeAnnotations(mapview.annotations)
    }
    
    //Al hacer que el delegate del mapview sea esta vista, los dos siguientes metodos se ejecutan automaticamente
    //Funcion que controla la pantallita que aparece cuando le aprietas al icono de la anotacion
    func mapView(mapView: MKMapView!, viewForAnnotation anotacion: MKAnnotation!) -> MKAnnotationView! {
        var pinView: MKPinAnnotationView = MKPinAnnotationView(annotation: anotacion, reuseIdentifier: "Custom")
        
        //Hacemos el cast para convertirlo a la clase MiAnotacion y poder acceder a todos sus atributos
        var miAnotacion = anotacion as MiAnotacion
        
        pinView.image = miAnotacion.icon
        pinView.canShowCallout = true
       

        var rightButton: UIButton = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIButton
        //...
        pinView.rightCalloutAccessoryView = rightButton
        
        var iconView = UIImageView(image: miAnotacion.icon)
        //...
        pinView.leftCalloutAccessoryView = iconView
        return pinView
    }
    
    //Funcion que muestra mas informacion sobre una anotacion
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) -> Void {
            var annotation = view.annotation as MiAnotacion
            var alert = UIAlertController(title: annotation.title, message: annotation.subtitle, preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title:"Aceptar", style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //Se crea este metodo para que los labels ya tengan un valor al iniciar la app
    func refreshPosition() {
            latitudeLabel.text = "Latitud: \(mapview.centerCoordinate.latitude)"
            longitudeLabel.text = "Longitud: \(mapview.centerCoordinate.longitude)"
    }
}

