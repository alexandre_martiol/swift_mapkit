//
//  SecondViewController.swift
//  MapKitApp
//
//  Created by Alexandre Martinez Olmos on 11/12/14.
//  Copyright (c) 2014 AlexM. All rights reserved.
//

import UIKit
import MapKit

class SecondViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var labelTitle: UITextField!
    @IBOutlet weak var labelSubtitle: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var typeOfAnnotation: NSString = "Restaurant"
    var iconSelected: UIImage!
    
    var arrayAnnotationTypes:[String] = ["Restaurant", "Cafe", "Pub", "Cinema"]
    var arrayIconImage = [UIImage (named: "restaurant_icon.gif"), UIImage (named: "cafe_icon.png"), UIImage (named: "pub_icon.png"), UIImage (named: "cinema_icon.png")]

    override func viewDidLoad() {
        super.viewDidLoad()

        //Relacionamos el delegate de la vista con el pickerview para que se ejecuten los metodos del pickerview automaticamente
        pickerView.delegate = self
        pickerView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func createAnnotation(sender: AnyObject) {
        self.crearAnotacion()
        
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    func crearAnotacion(){
        var point = MiAnotacion(c: mapview.centerCoordinate, t: self.labelTitle.text, st: self.labelSubtitle.text, ty: self.typeOfAnnotation, i: iconSelected)
        
        mapview.addAnnotation(point)
    }
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        //Devuelve el número de columnas del PickerView
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //Devuelve el número de opciones (filas) del PickerView
        return arrayAnnotationTypes.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        //Devuelve la palabra que debe mostrar el PickerView en la posición row
        return arrayAnnotationTypes[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Qué hago con el valor seleccionado del PickerView
        typeOfAnnotation = arrayAnnotationTypes[row]
        iconSelected = arrayIconImage[row]
    }
    
    //Esta funcion modifica el pickerview creando una vista editable para cada fila del mismo.
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var pickerCustomView: UIView = UIView(frame: CGRectMake(0.0, 0.0, pickerView.rowSizeForComponent(component).width - 10.0, pickerView.rowSizeForComponent(component).height))
        
        //Se crean los componentes de la vista y se les da el valor correspondiente. Finalmente se añaden a la vista y se retorna
        var pickerViewImage = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 20, height: 20))
        var pickerViewLabel = UILabel(frame: CGRect(x: 25.0, y: -5.0, width: pickerView.rowSizeForComponent(component).width - 10.0, height: pickerView.rowSizeForComponent(component).height))
        
        pickerViewImage.image = arrayIconImage[row]
        pickerViewLabel.text = arrayAnnotationTypes[row]
        
        pickerCustomView.addSubview(pickerViewImage)
        pickerCustomView.addSubview(pickerViewLabel)

        return pickerCustomView
    }
}
