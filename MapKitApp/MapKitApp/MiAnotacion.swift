//
//  MiAnotacion.swift
//  MapKitApp
//
//  Created by AlexM on 11/12/14.
//  Copyright (c) 2014 AlexM. All rights reserved.
//

import UIKit
import MapKit

class MiAnotacion: NSObject, MKAnnotation {
    var imagen: String = "pin.png"
    var title: NSString
    var subtitle: NSString
    var type: NSString
    var icon: UIImage
    var coordinate: CLLocationCoordinate2D
    
    func getTitle() -> NSString{
        return self.title
    }
    
    func getSubtitle () -> NSString{
        return self.subtitle
    }
    
    func getType () -> NSString{
        return self.type
    }
    
    init(c: CLLocationCoordinate2D, t: NSString, st: NSString, ty: NSString, i: UIImage){
        coordinate = c
        title = t
        subtitle = st
        icon = i
        type = ty
    }
}